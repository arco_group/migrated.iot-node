// -*- mode: c++; coding: utf-8 -*-

module IoT {

    interface NodeAdmin {
	void restart();
	void factoryReset();
    };

    interface IDMAdmin {
	void setIDMRouter(string proxy);
	void setIDMAddress(string address);
    };

    interface ISPublisherAdmin {
	void setTopicManager(string proxy);
    };

    interface WiFiAdmin {
	void setupWiFi(string ssid, string key);
    };

};
