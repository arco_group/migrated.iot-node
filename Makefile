# -*- mode: makefile-gmake; coding: utf-8 -*-

DESTDIR ?= ~
VER     ?= $(shell head -n 1 debian/changelog | cut -d' ' -f2 | tr -d "()")

all:

install:
	install -d $(DESTDIR)/usr/bin
	install -m 755 bin/node-client.py $(DESTDIR)/usr/bin/node-client

	install -d $(DESTDIR)/usr/share/slice/iot
	install -m 644 slice/node.ice $(DESTDIR)/usr/share/slice/iot


arduino-package: ARDUINO_DIST=/tmp/iot-node/src
arduino-package: clean update-versions
	$(RM) -rf $(ARDUINO_DIST)

	install -d $(ARDUINO_DIST)
	install -m 644 arduino/* $(ARDUINO_DIST)
	cd src; slice2c ../slice/IceStorm-min.ice --ice
	cp -r src $(ARDUINO_DIST)
	cp -r slice $(ARDUINO_DIST)

	cd $(ARDUINO_DIST); zip -r icec-iot-node-$(VER).zip *
	mv $(ARDUINO_DIST)/*.zip .
	cp icec-iot-node-$(VER).zip icec-iot-node-latest.zip

update-versions:
	sed -i -E "s/(^ *\"version\": *)(\".+\")/\1\"$(VER)\"/g" arduino/library.json


.PHONY: clean
clean:
	find -name "*.pyc" -print -delete
	find -name "*~" -print -delete
	$(RM) -f src/IceStorm-min.h
	$(RM) *.zip
