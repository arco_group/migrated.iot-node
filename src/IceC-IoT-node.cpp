// -*- mode: c++; coding: utf-8 -*-

#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>

#include <IceC.h>
#include <IceStorm-min.h>

#include "IceC-IoT-node.h"

#ifndef STATUS_LED
#define STATUS_LED LED_BUILTIN
#endif

Ticker async;

void
led_blink(byte count, byte timeout) {
    for (byte i=0; i<count; i++) {
        digitalWrite(STATUS_LED, HIGH);
        delay(timeout);
        digitalWrite(STATUS_LED, LOW);
        delay(timeout);
    }
}

void
reset_node() {
    Serial.println("Node: resetting...");
    delay(2000);
    ESP.restart();
}

void
factory_reset() {
    Serial.println("Node: erasing EEPROM...");
    IceC_Storage_clear();
    Serial.println("Node: EEPROM cleared! Restarting...");

    led_blink(5, 50);
    ESP.restart();
}

String
get_local_ip() {
    if (WiFi.getMode() == WIFI_AP)
        return WiFi.softAPIP().toString();
    else
        return WiFi.localIP().toString();
}

void
setup_ota() {
    bool led_state = LOW;
    ArduinoOTA.onStart([]() {
        Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
        digitalWrite(STATUS_LED, HIGH);
    });
    ArduinoOTA.onProgress([&led_state](unsigned int progress, unsigned int total) {
        byte percentage = (progress / (total / 100));
        led_state = percentage % 2;
        digitalWrite(STATUS_LED, led_state);
        Serial.printf("Progress: %u%%\r", percentage);
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
    ArduinoOTA.begin();
}

void
handle_ota() {
    ArduinoOTA.handle();
}

void
check_buttons() {
    if (digitalRead(PIN_BUTTON) == 1)
	   return;

    bool erase = false;
    bool led_state = false;
    long start = millis();
    while (digitalRead(PIN_BUTTON) == 0) {
        delay(100);

        if (millis() - start > 3000) {
            erase = true;
            digitalWrite(STATUS_LED, led_state);
            led_state = !led_state;
        }
    }

    if (erase)
        factory_reset();
}

// set to true if it is connected and can other objects
bool is_online;

void
_check_wifi_connected() {
    int c = 0;
    while (WiFi.status() != WL_CONNECTED) {
    	c++;
    	Serial.print(".");
    	check_buttons();

    	if (c > 60) {
    	    Serial.println("\nWiFi: connection failed! Rebooting...");
            reset_node();
    	}
    	delay(500);
    }

    is_online = true;
}

void
setup_wireless() {
    // get stored wireless settings
    char ssid[MAX_SSID_SIZE] = {0};
    IceC_Storage_get(DBINDEX_SSID, ssid, MAX_SSID_SIZE);

    // if available, try to connect
    if (ssid[0] != 0) {
	    char key[MAX_WPAKEY_SIZE];
	    IceC_Storage_get(DBINDEX_WPAKEY, key, MAX_WPAKEY_SIZE);

	    Serial.printf("WiFi: connecting to '%s'", ssid);
        WiFi.disconnect();
        WiFi.mode(WIFI_STA);
        WiFi.enableAP(false);
        WiFi.begin(ssid, key);

        _check_wifi_connected();
        Serial.println();
    }

    // if not available, config as AP
    else {
        String ssid = "node-" + String(ESP.getChipId());
        Serial.printf("WiFi: setting up AP as '%s'\n", ssid.c_str());

        WiFi.mode(WIFI_AP);
        WiFi.enableSTA(false);
        WiFi.softAP(ssid.c_str());
    }

    Serial.printf("WiFi: ready, IP address: ");
    Serial.println(get_local_ip());
}

void
async_restart_node() {
    Serial.println("Node: restarting...");
    async.once(1, []() { ESP.restart(); });
}

void
async_factory_reset() {
    async.once(1, []() { factory_reset(); });
}

void
store_wifi_settings(Ice_String ssid, Ice_String key) {
    const char zero = 0;
    IceC_Storage_put(DBINDEX_SSID, ssid.value, ssid.size);
    IceC_Storage_put(DBINDEX_SSID + ssid.size, &zero, 1);
    IceC_Storage_put(DBINDEX_WPAKEY, key.value, key.size);
    IceC_Storage_put(DBINDEX_WPAKEY + key.size, &zero, 1);

    char ssid_[ssid.size + 1];
    strncpy(ssid_, ssid.value, ssid.size);
    ssid_[ssid.size] = 0;

    char key_[key.size + 1];
    strncpy(key_, key.value, key.size);
    key_[key.size] = 0;

    Serial.printf("WiFi: stored settings, ssid: '%s', key: '%s'\n", ssid_, key_);
}

void
store_topic_manager(Ice_String proxy) {
    const char zero = 0;
    IceC_Storage_put(DBINDEX_IS_PROXY, proxy.value, proxy.size);
    IceC_Storage_put(DBINDEX_IS_PROXY + proxy.size, &zero, 1);

    char prx[proxy.size + 1];
    strncpy(prx, proxy.value, proxy.size);
    prx[proxy.size] = 0;

    Serial.printf("IS: set TopicManager to '%s'\n", prx);
}

bool
create_topic_manager(Ice_CommunicatorPtr ic, Ice_ObjectPrxPtr manager) {
    char strprx[MAX_PROXY_SIZE] = {0};
    IceC_Storage_get(DBINDEX_IS_PROXY, strprx, MAX_PROXY_SIZE);

    if (strprx[0] == 0) {
        Serial.println("Error: IS Manager not set!\n");
        return false;
    }

    Ice_Communicator_stringToProxy(ic, strprx, manager);
    return true;
}

bool
get_topic(Ice_ObjectPrxPtr manager, const char* name, Ice_ObjectPrxPtr topic) {
    new_Ice_String(topic_name, name);
    IceStorm_TopicManager_retrieve(manager, topic_name, topic);
    if (Ice_ObjectPrx_raisedException(manager, "::IceStorm::NoSuchTopic")) {
        IceStorm_TopicManager_create(manager, topic_name, topic);
    }

    if (Ice_ObjectPrx_raisedAnyException(manager))
        return false;
    return true;
}

bool
get_publisher(Ice_ObjectPrxPtr topic, Ice_ObjectPrxPtr publisher) {
    IceStorm_Topic_getPublisher(topic, publisher);

    if (Ice_ObjectPrx_raisedAnyException(topic))
        return false;
    return true;
}
