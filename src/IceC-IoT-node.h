// -*- mode: c++; coding: utf-8 -*-

#ifndef _NODE_UTILS_H_
#define _NODE_UTILS_H_

#include <Arduino.h>
#include <Ticker.h>

#include <IceC.h>

#define PIN_BUTTON          0

#define MAX_SSID_SIZE       32
#define MAX_WPAKEY_SIZE     64
#define MAX_PROXY_SIZE      64

#define DBINDEX_SSID        0
#define DBINDEX_WPAKEY      DBINDEX_SSID + MAX_SSID_SIZE
#define DBINDEX_IS_PROXY    DBINDEX_WPAKEY + MAX_WPAKEY_SIZE
#define DBINDEX_LAST        DBINDEX_IS_PROXY + MAX_PROXY_SIZE

// set to true if it is connected and can reach IceStorm
extern bool is_online;

// used to invoke async operations
extern Ticker async;

void led_blink(byte count, byte timeout);
void reset_node();
void factory_reset();
String get_local_ip();
void setup_wireless();
void setup_ota();
void handle_ota();
void check_buttons();

void async_restart_node();
void async_factory_reset();
void store_wifi_settings(Ice_String ssid, Ice_String key);
void store_topic_manager(Ice_String proxy);

bool create_topic_manager(Ice_CommunicatorPtr ic, Ice_ObjectPrxPtr manager);
bool get_topic(Ice_ObjectPrxPtr manager, const char* name, Ice_ObjectPrxPtr topic);
bool get_publisher(Ice_ObjectPrxPtr topic, Ice_ObjectPrxPtr publisher);

#endif /* _NODE_UTILS_H_ */
