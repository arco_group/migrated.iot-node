#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-

import sys
import os
import Ice
from argparse import ArgumentParser


slice_path = "../src/node.ice"
if not os.path.exists(slice_path):
    slice_path = "/usr/share/slice/iot/node.ice"

Ice.loadSlice("{} -I/usr/share/slice -I{} --all".format(slice_path, Ice.getSliceDir()))
import IoT  # noqa


class Client(Ice.Application):
    def run(self, args):
        if not self.parse_arguments(args[1:]):
            return

        self.create_proxies()
        self.exec_actions()

    def parse_arguments(self, args):
        parser = ArgumentParser()
        parser.add_argument(
            "proxy", type=str,
            help="Proxy of admin object on node")

        # NodeAdmin interface
        parser.add_argument(
            "--factory-reset", action="store_true",
            help="erase all settings and restart")
        parser.add_argument(
            "--restart", action="store_true",
            help="restart node")

        # IDMAdmin interface
        parser.add_argument(
            "--set-idm-address", dest="idm_address",
            help="set base IDM address for this node")
        parser.add_argument(
            "--set-idm-router", dest="idm_router",
            help="set proxy of IDM router")

        # ISPublisherAdmin interface
        parser.add_argument(
            "--set-topic-manager", dest="topic_manager",
            help="set proxy of IceStorm TopicManager")

        # WiFiAdmin interface
        parser.add_argument(
            "--set-wifi", nargs=2, dest="wifi", metavar=("SSID", "KEY"),
            help="setup WiFi settings")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True

    def create_proxies(self):
        ic = self.communicator()
        proxy = ic.stringToProxy(self.args.proxy).ice_encodingVersion(Ice.Encoding_1_0)

        self.node_prx = IoT.NodeAdminPrx.uncheckedCast(proxy)
        self.idm_prx = IoT.IDMAdminPrx.uncheckedCast(proxy)
        self.publisher_prx = IoT.ISPublisherAdminPrx.uncheckedCast(proxy)
        self.wifi_prx = IoT.WiFiAdminPrx.uncheckedCast(proxy)

    def exec_actions(self):
        if self.args.factory_reset:
            return self.factory_reset()

        if self.args.restart:
            return self.restart_node()

        if self.args.wifi is not None:
            return self.setup_wifi()

        if self.args.idm_router is not None:
            return self.set_idm_router()

        if self.args.idm_address is not None:
            return self.set_idm_base_address()

        if self.args.topic_manager is not None:
            return self.set_topic_manager()

    def factory_reset(self):
        print("- setting factory defaults on node...")
        self.node_prx.factoryReset()

    def restart_node(self):
        print("- restarting node...")
        self.node_prx.restart()

    def setup_wifi(self):
        print("- setting WiFi on node")
        ssid = self.args.wifi[0]
        key = self.args.wifi[1]
        self.wifi_prx.setupWiFi(ssid, key)

    def set_idm_router(self):
        print("- setting IDM router to '{}'".format(self.args.router))
        self.idm_prx.setIDMRouter(self.args.router)

    def set_idm_base_address(self):
        addr = self.args.idm_address.replace(":", "").strip()
        print("- setting IDM base address to '{}'".format(addr))
        self.idm_prx.setIDMAddress(addr)

    def set_topic_manager(self):
        print("- setting IceStorm TopicManager to '{}'".format(self.args.topic_manager))
        self.publisher_prx.setTopicManager(self.args.topic_manager)


if __name__ == '__main__':
    Client().main(sys.argv)
