// -*- mode: c++; coding: utf-8 -*-

#include <node.ice>

module Dummies {
    interface VTNode extends
	IoT::NodeAdmin,
	IoT::IDMAdmin,
	IoT::ISPublisherAdmin,
	IoT::WiFiAdmin {};
};
