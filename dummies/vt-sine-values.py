#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import os
import sys
import signal
import Ice
import IceStorm
from time import time, sleep
from libcitisim import logging, SmartObject, MetadataHelper
from itertools import cycle

pwd = os.path.dirname(__file__)
slice_dir = os.path.join(pwd, "../slice")
slice_path = os.path.join(pwd, "vtnode.ice")
Ice.loadSlice("{} --all -I{}".format(slice_path, slice_dir))
from Dummies import VTNode  # noqa

logger = logging.getLogger("vt-temperature")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


class NodeI(VTNode):

    def restart(self, current):
        logger.info("call to restart()")

    def factoryReset(self, current):
        logger.info("call to factoryReset()")

    def setIDMRouter(self, proxy, current):
        logger.info("call to setIDMRouter('{}')".format(proxy))

    def setIDMAddress(self, address, current):
        logger.info("call to setIDMAddress('{}')".format(address))

    def setTopicManager(self, proxy, current):
        logger.info("call to setTopicManager('{}')".format(proxy))

    def setupWiFi(self, ssid, key, current):
        logger.info("call to setupWiFi(ssid='{}', key='{}')".format(ssid, key))


class Publisher:
    def __init__(self, ic, topic_name, source):
        self.ic = ic
        self.source = source
        self.observer = self.get_publisher(topic_name)

    def get_publisher(self, topic_name):
        topic = self.get_topic(topic_name)
        pub = topic.getPublisher()
        return SmartObject.AnalogSinkPrx.uncheckedCast(pub)

    def get_topic(self, name):
        mgr = self.ic.propertyToProxy("TopicManager.Proxy")
        mgr = IceStorm.TopicManagerPrx.checkedCast(mgr)
        try:
            return mgr.retrieve(name)
        except IceStorm.NoSuchTopic:
            return mgr.create(name)

    def publish(self, value):
        meta = {
            "timestamp": int(time())
        }
        meta = MetadataHelper(**meta).to_dict()
        self.observer.notify(value, self.source, meta)
        logger.debug(
            "publish event to '{}', value: {}, source: {}, meta: {}"
            .format(self.observer, value, self.source, meta)
        )


class VTNodeApp(Ice.Application):
    def run(self, args):
        signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))

        self.min = int(self.get_property("Publisher.MinValue", "0"))
        self.max = int(self.get_property("Publisher.MaxValue", "15"))
        self.pool = cycle(list(range(self.min, self.max)) +
                          list(reversed(range(self.min, self.max))))

        self.shutdownOnInterrupt()
        self.register_servants()
        self.publish_events()

    def register_servants(self):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("Node.Adapter")
        adapter.activate()

        nodeid = self.get_property("Publisher.NodeID")
        nodeid = ic.stringToIdentity(nodeid)
        prx = adapter.add(NodeI(), nodeid)
        logger.info("Node servant: '{}'".format(prx))

    def publish_events(self):
        ic = self.communicator()
        topic = self.get_property("Publisher.Topic")
        delay = int(self.get_property("Publisher.Delay", "10"))
        source = self.get_property("Publisher.Source")
        publisher = Publisher(ic, topic, source)

        logger.info("Publising events on '{}'".format(topic))
        while True:
            try:
                publisher.publish(self.get_value())
                sleep(delay)
            except KeyboardInterrupt:
                break
        logger.info("Exiting...")

    def get_property(self, key, default=""):
        ic = self.communicator()
        value = ic.getProperties().getPropertyWithDefault(key, default)
        if value == "":
            raise ValueError("Missing configuration property: {}".format(key))
        return value

    def get_value(self):
        return next(self.pool)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} <topic>".format(sys.argv[0]))
        exit(1)

    exit(VTNodeApp(1).main(sys.argv))
